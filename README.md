# Catalyst HTML/CSS Intermediate training material

## Getting project set up on training machines

### Install nodejs & npm
Install nodejs and npm:
`sudo apt update`
`sudo apt install nodejs`

Then install npm:
`sudo apt install npm`

Check installation and versions:
`node -v`
`npm -v`

Install project:
)You may not need to chown the folder anymore) 
`sudo npm install -g gulp`
`sudo chown -R $(whoami) ~/.npm`
`npm install`

## Follow up email:

```
Hi everyone,

I hope you found the course useful. Here are a few links that I think you might find useful:

MDN web documentation:
https://developer.mozilla.org/en-US/docs/Web

SASS documentation:
https://sass-lang.com/documentation

ARIA
https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA

A guide to media queries:
https://developer.mozilla.org/en-US/docs/Web/CSS/Media_Queries/Using_media_queries

Gulp for beginners:
https://css-tricks.com/gulp-for-beginners/

'Can I use' browser support:
http://caniuse.com/

Flexbox:
https://css-tricks.com/snippets/css/a-guide-to-flexbox/

Learning game resources:
CSS selectors: https://flukeout.github.io/
Flexbox: http://flexboxfroggy.com/
CSS grid: http://cssgridgarden.com/

Thanks,

```
